/**
 * Created by johan on 30/11/13.
 */

/**
 * Stolen from http://stackoverflow.com/questions/1184624/convert-form-data-to-js-object-with-jquery
 */
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function loadJson(url, callback)
{
    $.ajax({
        url: url,
        type: "GET",
        success: callback
    });
}

function loadAccount(accountNumber, callback)
{
    loadJson('/rest.php/account/' + accountNumber, callback);
}

function restRequest(method, callback, restData, subUrl)
{
    if (typeof subUrl == 'undefined')
        subUrl = 'account';

    var request =
    {
        url: '/rest.php/' + subUrl,
        contentType: 'application/json',
        type: method,
        success: callback
    };

    if (restData !== null)
    {
        request.data = restData;
        request.processData = false;
    }
    $.ajax(request);

}

function doCreateAccount(account, callback)
{
    restRequest('POST', callback, account);
}

function doUpdateAccount(account, callback)
{
    restRequest('PUT', callback, account);
}

function doTransferMoney(transfer, callback)
{
    restRequest('POST', callback, transfer, 'transfer');
}

function doGetTransactions(account, date, callback)
{
    loadJson('/rest.php/transactions/' + account + '/' + date, callback);
}