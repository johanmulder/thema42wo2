/**
 * Created by johan on 30/11/13.
 */

function setContentFromUrl(url)
{
    $.ajax({
        url: url,
        type: "GET",
        success: function (resp) {
            $('#content').html(resp);
        }
    });
}

/**
 * Load the menu from a HTML snippet.
 * @param menuName
 */
function loadMenu(menuName)
{
    setContentFromUrl('snippets/' + menuName + '.html');
}

/**
 * Show account data.
 * @param account
 */
function showAccount(account)
{
    loadAccount(account, function (account)
    {
        $('#error').hide();
        if (typeof account.error == 'undefined')
        {
            $('#account_info').show();
            $('#accountNumber').html(account.accountNumber);
            $('#name').html(account.name);
            $('#address').html(account.address);
            $('#city').html(account.city);
            $('#balance').html(account.balance);
            $('#limit').html(account.limit);
        }
        else
        {
            $('#error').show();
            $('#account_info').hide();
            $('#error').html("An error occurred: (" + account.error.code + ") " + account.error.message);
        }
    });
}

function createAccount()
{
    var account = JSON.stringify($('form').serializeObject());
    $('#error').html('');
    doCreateAccount(account, function (response)
    {
        if (typeof response.error == 'undefined')
        {
            $('#status').html('Account has been created');
            $('#account_form').html('');
        }
        else
            $('#status').html("An error occurred: (" + response.error.code + ") " + response.error.message);
    });
}

function updateAccount(account)
{
    if (account != null)
    {
        loadAccount(account, function (account)
        {
            $('#error').hide();
            if (typeof account.error == 'undefined')
            {
                $('#accountNumber').val(account.accountNumber);
                $('#accountNr').val(account.accountNumber);
                $('#name').val(account.name);
                $('#address').val(account.address);
                $('#city').val(account.city);
                $('#balance').val(account.balance);
                $('#limit').val(account.limit);
                $('#account_info').show();
            }
            else
            {
                $('#error').show();
                $('#account_info').hide();
                $('#error').html("An error occurred: (" + account.error.code + ") " + account.error.message);
            }
        });
    }
    else
    {
        var account = JSON.stringify($('form').serializeObject());
        $('#error').html('');
        doUpdateAccount(account, function (response)
        {
            if (typeof response.error == 'undefined')
            {
                $('#status').html('Account has been modified');
                $('#account_info').hide();
            }
            else
                $('#status').html("An error occurred: (" + response.error.code + ") " + response.error.message);
        });
    }
}

function transferMoney()
{
    var transfer = JSON.stringify($('form').serializeObject());
    doTransferMoney(transfer, function (response)
    {
        if (typeof response.status != 'undefined')
        {
            if (response.status)
            {
                $('#status').html('Transaction executed');
                $('#transfer_form').html('');
            }
            else
                $('#status').html('Transaction not executed');
        }
        else if (typeof response.error != 'undefined')
        {
            $('#status').html(response.error.message);
        }
    });
}

function getTransactions(accountNumber, date)
{
    // Always clear the body of the transaction table.
    $('#tx_body').html('');
    $('#status').html('');
    doGetTransactions(accountNumber, date, function (response)
    {
        if (typeof response.error == 'undefined')
        {

            if (response.length == 0)
            {
                $('#status').html('No transactions found');
            }
            else
            {
                $('#tx_table').show();
                for (var i = 0; i < response.length; i++)
                {
                    var transaction = response[i];
                    $('#tx_body').append
                    (
                        '<tr>' +
                        '<td>' + transaction.id + '</td>' +
                        '<td>' + transaction.date + '</td>' +
                        '<td>' + transaction.srcAccount + '</td>' +
                        '<td>' + transaction.dstAccount + '</td>' +
                        '<td>€' + transaction.amount + '</td>' +
                        '</tr>'
                    );
                }
            }
        }
        else
        {
            $('#tx_table').hide();
            $('#status').html(response.error.message);
        }
    });
}