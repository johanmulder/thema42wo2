<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 30/11/13
 * Time: 20:13
 */

require_once __DIR__ . '/autoloader.php';
ini_set('display_errors', 'on');
ini_set('xdebug.overload_var_dump', '0');
ini_set('html_errors', 'off');

?>
<!doctype HTML>
<html>
	<head>
		<title>Bank application</title>
	</head>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script src="js/app.js"></script>
	<script src="js/rest.js"></script>

	<body>
		<h1>Bank application</h1>
		<nav>
			Menu:
			<ul>
				<li><a href="#" onclick="loadMenu('showAccount')">Show account</a></li>
				<li><a href="#" onclick="loadMenu('createAccount')">Create account</a></li>
				<li><a href="#" onclick="loadMenu('modifyAccount')">Modify account</a></li>
				<li><a href="#" onclick="loadMenu('transferMoney')">Transfer money</a></li>
				<li><a href="#" onclick="loadMenu('getTransactions')">Get transactions</a></li>
			</ul>
		</nav>

		<div id="content"></div>
	</body>
</html>