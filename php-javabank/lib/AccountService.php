<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 30/11/13
 * Time: 20:53
 */

class AccountService
{
	/**
	 * @var SoapClient
	 */
	private $soapClient;
	/**
	 * @var Logger
	 */
	private $Logger;

	public function __construct($wsdl_url = 'http://appel.local:8080/thema42wo2/JavaBankService?wsdl', Logger $logger = null)
	{
		$this->soapClient = new SoapClient($wsdl_url,
			array('trace' => true, 'wsdl_cache' => WSDL_CACHE_NONE));
		// Initialize logger.
		if ($logger == null)
			$logger = new Logger();
		$this->Logger = $logger;
	}

	public function createAccount($account)
	{
		try
		{
			$param = new stdclass();
			$param->account = $account;
			$this->soapClient->openAccount($param);
			$this->Logger->error($this->soapClient->__getLastRequest());
			$this->Logger->error($this->soapClient->__getLastResponse());
		}
		catch (Exception $e)
		{
			$this->Logger->error($this->soapClient->__getLastRequest());
			$this->Logger->error($this->soapClient->__getLastResponse());
			throw $e;
		}
	}

	public function updateAccount($account)
	{
		// !DRY
		try
		{
			$param = new stdclass();
			$param->account = $account;
			$this->soapClient->alterAccount($param);
			$this->Logger->error($this->soapClient->__getLastRequest());
			$this->Logger->error($this->soapClient->__getLastResponse());
		}
		catch (Exception $e)
		{
			$this->Logger->error($this->soapClient->__getLastRequest());
			$this->Logger->error($this->soapClient->__getLastResponse());
			throw $e;
		}

	}

	public function getAccount($accountNumber)
	{
		$params = new stdclass();
		$params->accountNumber = $accountNumber;
		$account = $this->soapClient->getAccount($params);
		if (is_object($account) && isset($account->return))
			return $account->return;
		throw new Exception("Invalid result received: " . $this->getVarDump($account));
	}

	public function transfer($src, $dst, $amount)
	{
		$params = new stdclass();
		$params->amount = $amount;
		$params->srcAccountNumber = $src;
		$params->dstAccountNumber = $dst;
		try
		{
			return $this->soapClient->transfer($params)->return;
		}
		catch (Exception $e)
		{
			$this->Logger->error($this->soapClient->__getLastRequest());
			$this->Logger->error($this->soapClient->__getLastResponse());
			throw $e;
		}
	}

	public function getTransactions($accountNumber, $date)
	{
		try
		{
			$p = new stdclass();
			$p->accountNumber = $accountNumber;
			$p->date = strftime('%Y-%m-%dT%H:%M:%S', strtotime($date));
			$res = $this->soapClient->getTransactions($p);
			if (isset($res->return))
				return $res->return;
			return [];
		}
		catch (Exception $e)
		{
			$this->Logger->error($this->soapClient->__getLastRequest());
			$this->Logger->error($this->soapClient->__getLastResponse());
			throw $e;
		}
	}

	/**
	 * Get a string representing the value of a var.
	 * @param $var
	 * @return string
	 */
	private function getVarDump($var)
	{
		ob_start();
		var_dump($var);
		$getDump = ob_get_contents();
		ob_end_clean();
		return $getDump;
	}

} 