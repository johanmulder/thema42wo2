<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 30/11/13
 * Time: 20:47
 */

class Rest
{
	/**
	 * @var AccountService
	 */
	private $accountService;

	public function __construct(AccountService $accountService)
	{
		$this->accountService = $accountService;
	}

	/**
	 * Handle the REST request.
	 */
	public function handleRequest()
	{
		$url_info = explode('/', $_SERVER['PATH_INFO']);
		$section = $url_info[1];
		header("Content-type: application/json");
		try
		{
			switch ($_SERVER['REQUEST_METHOD'])
			{
				case 'GET':
					switch ($section)
					{
						case 'account':
							echo json_encode($this->accountService->getAccount($url_info[2]));
							break;
						case 'transactions':
							echo json_encode($this->accountService->getTransactions($url_info[2], $url_info[3]));
							break;
					}
					break;
				case 'POST':
					switch ($section)
					{
						case 'account':
							$this->accountService->createAccount(json_decode(file_get_contents('php://input')));
							$status = new stdclass();
							$status->status = 'Account created';
							echo json_encode($status);
							break;
						case 'transfer':
							$transfer_info = json_decode(file_get_contents('php://input'));
							$status = new stdclass();
							$status->status = $this->accountService->transfer($transfer_info->src, $transfer_info->dst, $transfer_info->amount);
							echo json_encode($status);
							break;
					}
				case 'PUT':
					switch ($section)
					{
						case 'account':
							$this->accountService->updateAccount(json_decode(file_get_contents('php://input')));
							$status = new stdclass();
							$status->status = 'Account modified';
							echo json_encode($status);
							break;
					}
			}
		}
		catch (Exception $e)
		{
			$error = new stdclass();
			$error->error = new stdclass();
			$error->error->code = $e->getCode();
			$error->error->message = $e->getMessage();

			echo json_encode($error);
		}
	}

}