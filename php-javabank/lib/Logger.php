<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 30/11/13
 * Time: 20:58
 */

class Logger
{
	private $stderr;
	const LOGLEVEL_INFO = 'INFO';
	const LOGLEVEL_ERROR = 'ERROR';

	public function __construct()
	{
		$this->stderr = fopen('php://stderr', 'w');
	}

	public function __destruct()
	{
		fclose($this->stderr);
	}

	private function writeLog($level, $message)
	{
		fwrite($this->stderr, strftime('[%Y-%m-%d %H:%M:%S ') . $level . '] ' . $message . "\n");
	}

	public function info($message)
	{
		$this->writeLog(self::LOGLEVEL_INFO, $message);
	}

	public function error($message)
	{
		$this->writeLog(self::LOGLEVEL_ERROR, $message);
	}

} 