package nl.hanze.javabank.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nl.hanze.javabank.Transaction;

public class TransactionDaoImpl implements TransactionDao
{
	private final Connection connection;
	
	public TransactionDaoImpl(Connection connection)
	{
		this.connection = connection;
	}

	@Override
	public void create(Transaction tx)
	{
		try
		{
			System.err.println(getClass().getName() + "(create) connection is " + connection.toString());
			String query = "INSERT INTO transaction (date, src_account, dst_account, amount) VALUES(?, ?, ?, ?)";
			PreparedStatement st = connection.prepareStatement(query);
			st.setDate(1, new java.sql.Date(tx.getDate().getTime()));
			st.setString(2, tx.getSrcAccount());
			st.setString(3, tx.getDstAccount());
			st.setFloat(4, tx.getAmount());
			
			st.executeUpdate();
			st.close();
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public Transaction[] getTransactions(String accountNumber, Date after)
	{
		try
		{
			List<Transaction> transactions = new ArrayList<>();
			String query = "SELECT * FROM transaction WHERE (src_account = ? OR dst_account = ?) AND date >= ?";
			PreparedStatement st = connection.prepareStatement(query);
			st.setString(1, accountNumber);
			st.setString(2, accountNumber);
			st.setDate(3, new java.sql.Date(after.getTime()));
			st.execute();
			
			ResultSet rs = st.getResultSet();
			if (rs != null)
				while (rs.next())
				{
					Transaction tx = new Transaction();
					tx.setAmount(rs.getFloat("amount"));
					tx.setDate(new Date(rs.getDate("date").getTime()));
					tx.setId(rs.getInt("transaction_id"));
					tx.setSrcAccount(rs.getString("src_account"));
					tx.setDstAccount(rs.getString("dst_account"));
					transactions.add(tx);
				}
			
			Transaction[] txArray = new Transaction[transactions.size()];
			transactions.toArray(txArray);
			// Close the resultset and transaction.
			rs.close();
			st.close();
			return txArray;
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

}
