package nl.hanze.javabank;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "transaction")
@SuppressWarnings("serial")
public class Transaction implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "transaction_id")
	private long id;
	@Column(name = "date", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date date;
	@Column(name = "src_account", nullable = false)
	private String srcAccount;
	@Column(name = "dst_account", nullable = false)
	private String dstAccount;
	@Column(name = "amount")
	private float amount;

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public float getAmount()
	{
		return amount;
	}

	public void setAmount(float amount)
	{
		this.amount = amount;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getSrcAccount()
	{
		return srcAccount;
	}

	public void setSrcAccount(String srcAccount)
	{
		this.srcAccount = srcAccount;
	}

	public String getDstAccount()
	{
		return dstAccount;
	}

	public void setDstAccount(String dstAccount)
	{
		this.dstAccount = dstAccount;
	}
}
