package nl.hanze.javabank;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import nl.hanze.javabank.dao.AccountDao;
import nl.hanze.javabank.dao.AccountDaoImpl;
import nl.hanze.javabank.dao.TransactionDao;
import nl.hanze.javabank.dao.TransactionDaoImpl;

/**
 * Stateless EJB implementing the JavaBank interface.
 * 
 * @author Johan Mulder
 * 
 */
// @Stateless
public class JavaBankImpl implements JavaBank
{
	private final Connection connection;
	// The accountDao
	private AccountDao accountDao;
	private TransactionDao transactionDao;

	public JavaBankImpl()
	{
		try
		{
			InitialContext ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("JavaBank");
			connection = ds.getConnection();
			connection.setAutoCommit(false);
			accountDao = new AccountDaoImpl(connection);
			transactionDao = new TransactionDaoImpl(connection);
		}
		catch (SQLException | NamingException e)
		{
			throw new RuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#openAccount(nl.hanze.javabank.Account)
	 */
	@Override
	public void openAccount(Account account)
	{
		// Check if the accounts exists already.
		Account existing = accountDao.getByNumber(account.getAccountNumber());
		if (existing != null)
			throw new RuntimeException("Account " + account.getAccountNumber() + " exists already");
		accountDao.create(account);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#transfer(float, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean transfer(float amount, String srcAccountNumber, String dstAccountNumber)
	{
		try
		{
			// Get source and destination account.
			Account src = accountDao.getByNumber(srcAccountNumber);
			Account dst = accountDao.getByNumber(dstAccountNumber);
			if (src == null)
				throw new Exception("Source account " + srcAccountNumber + " not found");
			if (dst == null)
				throw new Exception("Destination account " + dstAccountNumber + " not found");
			
			// Don't transfer if the current balance is lower than the given
			// amount,
			// or if the amount exceeds the limit of the account.
			if (src.getBalance() < amount || amount > src.getLimit())
			{
				System.err.println("The amount to transfer (" + amount + 
						") exceeds the limit of account " + src.getAccountNumber() + 
						"(" + src.getLimit() + ")");
				return false;
			}
			// Subtract the amount at the source account.
			src.setBalance(src.getBalance() - amount);
			// And add it to the destination account
			dst.setBalance(dst.getBalance() + amount);
			// Create a new transaction.
			Date curTime = new Date();
			Transaction tx = new Transaction();
			tx.setAmount(amount);
			tx.setDate(curTime);
			tx.setSrcAccount(src.getAccountNumber());
			tx.setDstAccount(dst.getAccountNumber());
			
			// Start transaction.
			connection.setAutoCommit(false);
			
			// Update the accounts.
			accountDao.update(src);
			// Uncomment to demonstrate atomicity of the transaction.
//			if (true) throw new RuntimeException("Test");
			accountDao.update(dst);
			// Create the transaction.
			transactionDao.create(tx);

			// Commit the changes.
			connection.commit();
			return true;
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#getAccount(java.lang.String)
	 */
	@Override
	public Account getAccount(String accountNumber)
	{
		return accountDao.getByNumber(accountNumber);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#alterAccount(nl.hanze.javabank.Account)
	 */
	@Override
	public void alterAccount(Account account)
	{
		accountDao.update(account);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see nl.hanze.javabank.JavaBank#getTransactions(java.lang.String,
	 * java.sql.Date)
	 */
	@Override
	public Transaction[] getTransactions(String accountNumber, Date date)
	{
		// Check if the account exists.
		if (accountDao.getByNumber(accountNumber) == null)
			throw new RuntimeException("Account " + accountNumber + " does not exist");
		// Get all transactions for the given date.
		return transactionDao.getTransactions(accountNumber, date);
	}

}
