package nl.hanze.cjibbank;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 * Local Session Bean implementation class ClearingHouseBean
 */
@Stateless
@Local(ClearingHouse.class)
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class ClearingHouseBean implements ClearingHouse
{
	// The session context of this EJB.
	@Resource
	private SessionContext sessionContext;
	// The entity manager of the JavaBank.
	@PersistenceContext(unitName = "JavaBank")
	private EntityManager emJavaBank;
	// The entity manager of the CJIBBank.
	@PersistenceContext(unitName = "CJIBBank")
	private EntityManager emClearingHouse;

	/**
	 * @see ClearingHouse#transferFromJavaBank(String, String, float)
	 */
	public void transferFromJavaBank(String srcAccountNumber, String dstAccountNumber, float amount)
	{
		transferMoney(srcAccountNumber, dstAccountNumber, amount, nl.hanze.javabank.Account.class);
	}
	
	/**
	 * @see ClearingHouse#transferToJavaBank(String, String, float)
	 */
	public void transferToJavaBank(String srcAccountNumber, String dstAccountNumber, float amount)
	{
		transferMoney(srcAccountNumber, dstAccountNumber, amount, nl.hanze.cjibbank.Account.class);
	}

	/**
	 * Transfer money. The source account is determined by the class type of the
	 * source account.
	 * 
	 * @param javaBankAccountNumber
	 * @param cjibBankAccountNumber
	 * @param amount
	 * @param srcAccountType
	 */
	private void transferMoney(String javaBankAccountNumber, String cjibBankAccountNumber,
			float amount, @SuppressWarnings("rawtypes") Class srcAccountType)
	{
		UserTransaction tx = sessionContext.getUserTransaction();
		try
		{
			// Start the transaction.
			tx.begin();

			// Get the JavaBank account.
			nl.hanze.javabank.Account javaBankAccount = emJavaBank.find(
					nl.hanze.javabank.Account.class, javaBankAccountNumber);
			if (javaBankAccount == null)
				throw new Exception("JavaBank account " + javaBankAccountNumber + " not found");
			// And the CJIB account.
			nl.hanze.cjibbank.Account cjibBankAccount = emClearingHouse.find(
					nl.hanze.cjibbank.Account.class, cjibBankAccountNumber);
			if (cjibBankAccount == null)
				throw new Exception("CJIB account " + cjibBankAccountNumber + " not found");
			// Transfer the money.
			if (javaBankAccount.getClass() == srcAccountType)
			{
				// The java bank account is the source account.
				// Check if there's enough money available in the source account
				if ((javaBankAccount.getBalance() - amount) < 0)
					throw new Exception("Not enough money available in account "
							+ javaBankAccount.getAccountNumber());
				javaBankAccount.setBalance(javaBankAccount.getBalance() - amount);
				cjibBankAccount.setBalance(cjibBankAccount.getBalance() + amount);
				// Create transactions.
				createJavaBankTransaction(javaBankAccountNumber, cjibBankAccountNumber, amount);
				createCjibBankTransaction(cjibBankAccountNumber, javaBankAccountNumber, amount);
			}
			else if (cjibBankAccount.getClass() == srcAccountType)
			{
				if ((cjibBankAccount.getBalance() - amount) < 0)
					throw new Exception("Not enough money available in account "
							+ cjibBankAccount.getAccountNumber());
				cjibBankAccount.setBalance(cjibBankAccount.getBalance() - amount);
				javaBankAccount.setBalance(javaBankAccount.getBalance() + amount);
				// Create transactions.
				createJavaBankTransaction(cjibBankAccountNumber, javaBankAccountNumber, amount);
				createCjibBankTransaction(javaBankAccountNumber, cjibBankAccountNumber, amount);
			}
			else
				// We should never get here.
				throw new IllegalArgumentException("Invalid source account class given: "
						+ srcAccountType.getCanonicalName());

			// Commit the transaction.
			tx.commit();
		}
		catch (Exception e)
		{
			rollback(tx);
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	private void createCjibBankTransaction(String srcAccount, String dstAccount, float amount)
	{
		nl.hanze.cjibbank.Transaction tx = new nl.hanze.cjibbank.Transaction();
		tx.setAmount(amount);
		tx.setDate(new Date());
		tx.setSrcAccount(srcAccount);
		tx.setDstAccount(dstAccount);
		emClearingHouse.persist(tx);
	}

	private void createJavaBankTransaction(String srcAccount, String dstAccount, float amount)
	{
		nl.hanze.javabank.Transaction tx = new nl.hanze.javabank.Transaction();
		tx.setAmount(amount);
		tx.setDate(new Date());
		tx.setSrcAccount(srcAccount);
		tx.setDstAccount(dstAccount);
		emJavaBank.persist(tx);
	}

	/**
	 * Roll back a user transaction.
	 * @param tx
	 */
	private void rollback(UserTransaction tx)
	{
		try
		{
			tx.rollback();
		}
		catch (IllegalStateException | SecurityException | SystemException e1)
		{
			e1.printStackTrace();
			throw new RuntimeException(e1.getMessage(), e1);
		}
	}

}
