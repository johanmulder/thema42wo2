package nl.hanze.cjibbank;

public interface ClearingHouse
{
	/**
	 * Transfer a sum of money from the JavaBank account number to the CJIBBank account number.
	 * @param srcAccountNumber The JavaBank account number.
	 * @param dstAccountNumber The CJIBBank account number.
	 * @param amount The amount of money to transfer.
	 */
	public void transferFromJavaBank(String srcAccountNumber, String dstAccountNumber, float amount);
	
	/**
	 * Transfer a sum of money from the CJIBBank account number to the JavaBank account number.
	 * @param srcAccountNumber The JavaBank account number.
	 * @param dstAccountNumber The CJIBBank account number.
	 * @param amount The amount of money to transfer.
	 */
	public void transferToJavaBank(String srcAccountNumber, String dstAccountNumber, float amount);
}
