package nl.hanze.cjibbank.webservice;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import nl.hanze.cjibbank.ClearingHouseBean;

@WebService(serviceName = "ClearingHouse")
public class ClearingHouse
{
	@EJB
	private ClearingHouseBean clearingHouseBean;

	/**
	 * 
	 * @param srcAccountNumber
	 * @param dstAccountNumber
	 * @param amount
	 * @return
	 * @throws Exception
	 */
	@WebMethod
	public boolean transferFromJavaBank(
			@WebParam(name = "srcAccountNumber") String srcAccountNumber,
			@WebParam(name = "dstAccountNumber") String dstAccountNumber,
			@WebParam(name = "amount") float amount) throws Exception
	{
		try
		{
			clearingHouseBean.transferFromJavaBank(srcAccountNumber, dstAccountNumber, amount);
			return true;
		}
		catch (EJBException e)
		{
			if (e.getCause() instanceof Exception)
				throw (Exception) e.getCause();
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 
	 * @param srcAccountNumber
	 * @param dstAccountNumber
	 * @param amount
	 * @return
	 * @throws Exception
	 */
	@WebMethod
	public boolean transferToJavaBank(
			@WebParam(name = "srcAccountNumber") String srcAccountNumber,
			@WebParam(name = "dstAccountNumber") String dstAccountNumber,
			@WebParam(name = "amount") float amount) throws Exception
	{
		try
		{
			clearingHouseBean.transferToJavaBank(srcAccountNumber, dstAccountNumber, amount);
			return true;
		}
		catch (EJBException e)
		{
			if (e.getCause() instanceof Exception)
				throw (Exception) e.getCause();
			throw new RuntimeException(e);
		}
	}
	
}
