package nl.hanze.cjibbank;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

// We need a different name for this entity, because otherwise we'd have a duplicate entity.
@Entity(name = "CJIBAccount")
@Table(name = "account")
@SuppressWarnings("serial")
public class Account implements Serializable
{
	@Id
	@Column(name = "account_number", nullable = false)
	private String accountNumber;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "address", nullable = false)
	private String address;
	@Column(name = "city", nullable = false)
	private String city;
	@Column(name = "`limit`", nullable = false)
	private float limit = 0;
	@Column(name = "balance", nullable = false)
	private float balance;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public float getLimit()
	{
		return limit;
	}

	public void setLimit(float limit)
	{
		this.limit = limit;
	}

	public float getBalance()
	{
		return balance;
	}

	public void setBalance(float balance)
	{
		this.balance = balance;
	}

	public String getAccountNumber()
	{
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}
}
